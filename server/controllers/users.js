import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import UserModel from '../models/user';

export const signIn = async (req, res) => {
    const {email, password} = req.body;

    try {
        const existingUser = await UserModel.findOne({email});
        if (!existingUser) return res.status(404).json({message: "User does'nt exit."})

        const isPasswordCorrect = await bcrypt.compare(password, existingUser.password);
        if (!isPasswordCorrect) return res.status(400).json({message: "Invalid credentials."})

        const token = jwt.sign({
            email: existingUser.email,
            id: existingUser._id
        }, process.env.SECRET_JWT, {expiresIn: "1h"})

        res.status(200).json({result: existingUser, token})

    } catch (e) {
        res.status(500).json({message: "Something went wrong"});
    }

}

export const signUp = async (req, res) => {
    const {email, password, firstName, lastName, confirmPassword} = req.body;

    try {
        const existingUser = await UserModel.findOne({email});

        if (existingUser) return res.status(400).json({message: "This user already exist"});

        if (password !== confirmPassword) return res.statut(400).json({message: "Passwords dont match"});

        const hashedPassword = await bcrypt.hash(password, 12);
        const result = await UserModel.create({email, password: hashedPassword, name: `${firstName} ${lastName}`});
        const token = jwt.sign({email: result.email, id: result._id}, process.env.SECRET_JWT, {expiresIn: "1h"});

        res.status(200).json({result, token});

    } catch (e) {
        res.status(500).json({message: "Something went wrong"});
    }
}
