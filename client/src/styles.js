import {makeStyles} from "@material-ui/core";


export default makeStyles((theme) => ({
    appBar: {
        borderRadius: 5,
        margin: '30px 0',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    heading: {
        color: '#2f566d',
    },
    image: {
        marginLeft: '15px',
    },
    [theme.breakpoints.down('xs')]: {

        mainContainer: {
            flexDirection: 'column-reverse'
        }
    }
}));