import {AUTH} from "../constants/actionTypes";
import * as api from '../api/index';

export const signIn = (formData, history) => async dispatch => {
    try {
        const {data} = await api.signIn(formData);
        dispatch({
            type: AUTH,
            data
        })
        // login user
        history.push('/')
    } catch (e) {
        console.log(e)
    }
}

export const signUp = (formData, history) => async dispatch => {
    try {
        const {data} = await api.signUp(formData);
        dispatch({
            type: AUTH,
            data
        })
        // signup user
        history.push('/')
    } catch (e) {
        console.log(e)
    }
}