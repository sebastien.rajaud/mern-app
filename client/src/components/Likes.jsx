import React from 'react';
import ThumbUpAltIcon from "@material-ui/icons/ThumbUpAlt";
import ThumbUpAltOutlined from "@material-ui/icons/ThumbUpAltOutlined";


/**
 *
 * @param data Array of datas
 * @returns {JSX.Element}
 * @constructor
 */
const Likes = ({data}) => {
    const user = JSON.parse(localStorage.getItem('profile'))
    if(data.likes.length > 0 ) {
        return data.likes.find( like => like === (user?.result?._id || user?.result?.googleId)) ? (
            <><ThumbUpAltIcon /> &nbsp; {data.likes.length >= 2 ? `You and ${data.likes.length - 1} others likes` : `You like`}  </>
        ) : (

            <><ThumbUpAltOutlined /> &nbsp; {data.likes.length > 1 ? `${data.likes.length} likes` : `${data.likes.length} like`} </>
        )
    }
    return  <><ThumbUpAltIcon fontSize="small"/>&nbsp;Like</>

}

export default Likes;